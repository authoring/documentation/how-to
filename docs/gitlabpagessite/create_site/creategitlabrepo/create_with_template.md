# Option A. Create a documentation site using a GitLab template

This solution is the simplest one since the repository will be preconfigured to use specific SSG (Static Site Generator) and the only thing for you to do will be to write the documentation content.

Create a new project by clicking on the upper right button **New project** and select the option **Create from template**. This will show the list of the templates which are available. 

!!! warning 
    Some of these templates from the displayed list are not for static sites, so choose one of the following:

    - Pages/Gatsby
    - Pages/Hugo
    - Pages/Jekyll
    - Pages/Plain HTML
    - Pages/GitBook
    - Pages/Hexo
    - Static Site Editor/Middleman
    - Middleman project with Static Site Editor support

    This list is continuously updated with the new alternatives available.

Once you know which SSG you would like to use just click on **Use template** button and fill in the form that will appear.

![create](/images/gitlab-create-template.png)

!!! warning
    The visibility level applies only to the git repository. This only affects who has access to the files in the repository.

* "Private" means you have to explicitly give read access to other users.
* "Internal" means all CERN users have read access.
* "Public" means that everyone on the Internet has read access.

Once you have filled in the form click on **Create project** button and the repository will be created with a documentation sample that you can easily edit.

More information about GitLab templates [here](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_new_project_template.html){target=_blank}.


!!! note
    GitLab has not ready-to-use templates for all SSG that it supports. If you are interested in [any other template that GitLab offers](https://gitlab.com/pages){target=_blank} and that is not listed in the previous available templates list you can [import it](/gitlabpagessite/create_site/creategitlabrepo/create_with_mkdocs/#a-import-project) using as **Git repository URL** the repository's example you want to use, for example, [the Harp template](https://gitlab.com/pages/harp).


#### Build artifacts

The last step is to build and push the artifacts which will be served to the users. For this, go to CI/CD section in the left panel of your repository and click on the Pipelines option. Once there, click on the upper right button **Run Pipeline**. A new screen will appear asking you to select the branch and set variables (by default, the pipeline should be run for the [repository's default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html#change-the-default-branch-name-for-a-project){target=_blank}). Ignore it and click on **Run Pipeline** button.

![pipelines_vars](/images/gitlab-pipelines-vars.png)


Wait until the job ends successfully.

![pipelines](/images/gitlab-pipelines-migration.png)


## Next step 

Good! your repository is configured. Now you can go to the next step to [create a site in Web Services](/gitlabpagessite/create_site/create_webeos_project/).
