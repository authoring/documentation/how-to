# Deploy your site locally

!!! note
    This alternative is only for sites that are using [Markdown along with MkDocs](/gitlabpagessite/create_site/creategitlabrepo/create_with_mkdocs/){target=_blank}. You can [check here the requirements](https://www.mkdocs.org/user-guide/installation/).

If you have or foresee to have or manage several documentation sites, you might be interested in setting up a local environment to develop and test them in an easy way.
You can easily achieve it by installing [MkDocs](https://www.mkdocs.org/getting-started/){target=_blank} and [MkDocs material](https://squidfunk.github.io/mkdocs-material/getting-started/){target=_blank}. Once you have all the dependencies installed you can simply run `mkdocs serve` in the directory with your documentation files. This will run a simple web server that can serve your files in the browser. With that approach you can validate your changes before pushing them to GitLab and there is no need to [create a new test/preview site](/gitlabpagessite/review/create_test_site/).

!!! note
    You do not need to deal with Gitlab CI configuration for this use case, only with your documentation files.
