# Callouts

If enabled, you can insert callouts like this:

!!! Tip
    This is a tip

!!! Note
    And this a note.

You can have more types of callouts, the list is [here](https://squidfunk.github.io/mkdocs-material/reference/admonitions/).

## Add the extension

In `mkdocs.yml`, add the following list element to `markdown-extensions:`

```
- admonition
```

To enable nesting code blocks, it's recommended to also add "superfences":

```
- pymdownx.superfences
```

So your code block would look like this:

```
markdown_extensions:
  - admonition # callouts like !!!note and !!!tip
  - pymdownx.superfences # code inside admonitions
```
