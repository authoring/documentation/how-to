# Enabling Latex equations in your documentation

With this, you can format
` $\frac{1}{\sin(x)}$ `
 as
 ![](../images/latex_example.png).

Credit: Michal Kolodziejski

## Add Javascript config file

You need a `docs/javascript/config.js` file.

!!! tip
    If you don't yet have it, create a `javascript` subdirectory under `docs` and create a new file `config.js`.

Add to its content:

```
window.MathJax = {
  tex: {
    inlineMath: [["\\(", "\\)"]],
    displayMath: [["\\[", "\\]"]],
    processEscapes: true,
    processEnvironments: true
  },
  options: {
    ignoreHtmlClass: ".*|",
    processHtmlClass: "arithmatex"
  }
};

document$.subscribe(() => {
  MathJax.typesetPromise()
})
```

## Add the `pymdownx.arithmatex` extension

In the `mkdocs.yml` file, under `markdown_extensions`, add the following:

```
markdown_extensions:
  - pymdownx.arithmatex:
      generic: true
```

!!! tip
    If you don't have any extension(s) yet, paste the whole code block above to the end of `mkdocs.yml`

## Include the `config.js` script

Add the following code block also to the end of `mkdocs.yml`

```
extra_javascript:
  - javascripts/config.js
  - https://polyfill-fastly.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
```

After pushing the commit, you'll be able to write Latex.
