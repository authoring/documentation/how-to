# Multi-language support i18n

It is possible to publish your documentation in different languages using the [plugin mkdocs-static-i18n](https://github.com/ultrabug/mkdocs-static-i18n).

In order to make your documentation available in multiple languages you need to make sure that the mkdocs configuration file ([`mkdocs.yml`](https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-example/blob/master/mkdocs.yml)) contains necessary configuration options. The snippet below shows how to declare that your documentation site will be available in three languages.
```yaml
extra:
  alternate:
    - name: English
      link: ./
      lang: en
    - name: Français
      link: ./fr/
      lang: fr
    - name: Galego
      link: ./gl/
      lang: gl
```

Furthermore, you have to add the plugin `i18n` to the list of plugins used, along with the default language and the list of languages that your documentation is available in. You can [check the available languages here](https://squidfunk.github.io/mkdocs-material/setup/changing-the-language/#configuration). In addition, it is possible to translate section titles in the navigation:
```yaml
plugins:
  - search
  - i18n:
    default_language: en
    languages:
      en: english
      fr: français
      gl: galego
    nav_translations:
      en:                           # Translate section titles into English
        Chapter1: Chapter 1
        Subchapter1: Chapter 1.1
        Subchapter1a: Chapter 1.1.1
        Chapter2: Chapter 2
        Interesting links: Links to resources
      fr:                           # Translate section titles into French
        Chapter1: Chapitre 1
        Subchapter1: Chapitre 1.1
        Subchapter1a: Chapitre 1.1.1
        Chapter2: Chapitre 2
        Interesting links: Liens vers des ressources
      gl:                           # Translate section titles into Galician
        Chapter1: Capítulo 1
        Subchapter1: Capítulo 1.1
        Subchapter1a: Capítulo 1.1.1
        Chapter2: Capítulo 2
        Interesting links: Links a fontes
```

!!! warning
    Notice that all your files have to follow the naming format `<file_name>.<language>.md` in order for them to be placed on the correct language page.
    For those who are updating their sites notice that the existence of files without the `<language>` extension like `index.md` will lead to a buggy site. **If you want to deploy a multilanguage site please, follow the naming format `<file_name>.<language>.md`**.

    **IMPORTANT**: Notice that, from the sections mapping tuples of the MkDocs configuration file `mkdocs.yml`  (i.e.: `Chapter1: Chapter 1` in the previous example), the first element of the tuple (`Chapter1`) is the name of the folder (inside `docs`) that contains your files for this section. This mapping is case sensitive and always starts with a capital letter (`Chapter1`, `Subchapter1a`, etc.) even if you folder name starts by a lowercase letter as in this example. Also, underscores `_` on folder's name are translated into spaces on `mkdocs.yml` mapping (see that `interesting_links` folder corresponds with `Interesting links:` section). It is mandatory to follow this convention, otherwise, the nav title will not be translated.
